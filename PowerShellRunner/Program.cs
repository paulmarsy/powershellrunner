﻿using System;
using System.Management.Automation.Runspaces;
using System.Windows.Forms;

namespace PowerShellRunner
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main(string[] args)
        {
            try
            {
                var runspace = RunspaceFactory.CreateRunspace();
                runspace.Open();
                var pipeline = runspace.CreatePipeline();
                pipeline.Commands.AddScript(string.Join(" ", args));
                pipeline.Invoke();
            }
            catch (Exception e)
            {
                MessageBox.Show(e.Message);
            }
        }
    }
}
